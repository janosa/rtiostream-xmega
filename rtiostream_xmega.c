/*
 * rtiostream_xmega.c
 *
 * Created: 24.03.2014 14:53:26
 *  Author: janov_000
 */ 

#include <avr/io.h>
#include <avr/interrupt.h>
#include <stdlib.h>
#include <string.h>

#include "rtiostream.h"
#include "dma_driver.h"

// TX Channels
#define DMA_TX_CHANNEL1 &DMA.CH0
#define DMA_TX_CHANNEL2 &DMA.CH1

// RX Channels
#define DMA_RX_CHANNEL1 &DMA.CH2
#define DMA_RX_CHANNEL2 &DMA.CH3

#define BUFFER_SIZE 512

typedef struct {
	volatile DMA_CH_t* ch;
	union
	{
		uint8_t completed; // Used by receiving channels to mark that the channel is fully read
		uint8_t configured; // Used by transmitting channels to indicate that they should be sent
	};
	
	uint16_t read; // Used by receiving channels
	char buffer[BUFFER_SIZE];
} data_channel_t;

// Dual buffers for both RX and TX
static data_channel_t TX_DC[2];
static data_channel_t RX_DC[2];

// BSEL and BSCALE values for 256000 baud at 32MHz
#define BSEL 872
#define BSCALE 0b1001

static volatile uint8_t activeReceiveChannel = 0;

void SetupTransmitChannel(data_channel_t* data_channel, size_t size );
void SetupReceiveChannel(data_channel_t* data_channel );

RTIOSTREAMAPI int rtIOStreamOpen(
int    argc,
void * argv[]
)
{
	// Init the uart tp 115200 bps
	USARTC0.CTRLC |= USART_CHSIZE_8BIT_gc;
	USARTC0.BAUDCTRLA = (BSEL&0xFF);
	USARTC0.BAUDCTRLB = ((BSEL>>8)&0b1111);
	USARTC0.BAUDCTRLB |= (BSCALE<<4);
	
	PORTC.DIRSET = PIN3_bm;
	PORTC.DIRCLR = PIN2_bm;
	USARTC0.CTRLB |= USART_RXEN_bm | USART_TXEN_bm;
	
	// Enable the DMA controller
	DMA_Enable();
	
	// Set up the channels
	RX_DC[0].ch = DMA_RX_CHANNEL1;
	RX_DC[0].read = BUFFER_SIZE;
	RX_DC[1].ch = DMA_RX_CHANNEL2;
	RX_DC[1].read = BUFFER_SIZE;
	
	TX_DC[0].ch = DMA_TX_CHANNEL1;
	TX_DC[0].configured = 0;
	TX_DC[1].ch = DMA_TX_CHANNEL2;
	TX_DC[1].configured = 0;
	
	// Set up the the receive channels
	SetupReceiveChannel(&RX_DC[0]);
	SetupReceiveChannel(&RX_DC[1]);
	
	// Enable the receive channel
	activeReceiveChannel = 0;
	// Enable double buffering for the RX channels
	DMA_ConfigDoubleBuffering(DMA_DBUFMODE_CH23_gc);
	DMA_EnableChannel(RX_DC[0].ch);	
	//DMA_EnableChannel(RX_DC[1].ch);	
	
	return 0;
}

RTIOSTREAMAPI int rtIOStreamClose(
int streamID
) 
{
	// We don't do that shit.
	return 0;
}

RTIOSTREAMAPI int rtIOStreamSend(
int          streamID,
const void * src,
size_t       size,
size_t     * sizeSent
)
{	
	/*
	for(uint16_t i = 0; i < size; i++) 
	{
		while(!(USARTC0.STATUS & USART_DREIF_bm));
		USARTC0.DATA = ((uint8_t*)src)[i];
	}
	*sizeSent = size;
	return RTIOSTREAM_NO_ERROR;*/
	data_channel_t* dc = NULL;
	if( TX_DC[0].configured == 0 && !(TX_DC[0].ch->CTRLA & DMA_CH_ENABLE_bm)) 
	{
		dc = &TX_DC[0];
	}
	else if( TX_DC[1].configured == 0  && !(TX_DC[1].ch->CTRLA & DMA_CH_ENABLE_bm)) 
	{
		dc = &TX_DC[1]; 
	}
	else
	{
		// No channels available.
		*sizeSent = 0;
		return RTIOSTREAM_NO_ERROR;
	}
	
	size_t send = size;
	
	// Copy over the data to be sent
	if( size > BUFFER_SIZE ) 
	{
		send = BUFFER_SIZE;		
	} 
	memcpy(dc->buffer, src, send);
	SetupTransmitChannel(dc, send);
	// We should enable the channel if none of the channels is currently running
	if(!(TX_DC[0].ch->CTRLA & DMA_CH_ENABLE_bm) && !(TX_DC[1].ch->CTRLA & DMA_CH_ENABLE_bm)) 
	{
		DMA_EnableChannel(dc->ch);		
	}
	
	*sizeSent = send;
	return RTIOSTREAM_NO_ERROR;
}

RTIOSTREAMAPI int rtIOStreamRecv(
int      streamID,
void   * dst,
size_t   size,
size_t * sizeRecvd
) 
{
	data_channel_t* dc = &RX_DC[activeReceiveChannel];
	// Calculate how much we can read
	uint16_t canRead = BUFFER_SIZE - dc->read - dc->ch->TRFCNT;
	uint8_t isOngoing = dc->ch->CTRLA & DMA_CH_ENABLE_bm; //DMA_CH_IsOngoing(dc->ch);
	if( !isOngoing ) 
	{
		// The DMA channel is completed
		canRead = BUFFER_SIZE - dc->read;
		// Signify that we 
	}
	
	uint16_t read = canRead;
	if (size < canRead)
	{
		read = size;
	}
	
	if (read == 0)
	{
		*sizeRecvd = 0;
		if( !isOngoing )
		{
			// Reset the channel to the original address
			dc->read = 0;
			dc->ch->DESTADDR0 = (( (uint16_t) dc->buffer) >> 0*8 ) & 0xFF;
			dc->ch->DESTADDR1 = (( (uint16_t) dc->buffer) >> 1*8 ) & 0xFF;
			dc->ch->DESTADDR2 = 0;
			
			activeReceiveChannel = activeReceiveChannel == 0 ? 1 : 0;
			
		}
		return RTIOSTREAM_NO_ERROR;
	}
	
	if(read + dc->read > BUFFER_SIZE) {
		return RTIOSTREAM_ERROR;
	}
	
	// Copy over the data
	memcpy(dst, &dc->buffer[dc->read], read);
	dc->read += read;
	
	*sizeRecvd = read;
	if( !isOngoing ) 
	{
		// Reset the channel to the original address
		dc->read = 0;
		dc->ch->DESTADDR0 = (( (uint16_t) dc->buffer) >> 0*8 ) & 0xFF;
		dc->ch->DESTADDR1 = (( (uint16_t) dc->buffer) >> 1*8 ) & 0xFF;
		dc->ch->DESTADDR2 = 0; 
		
		activeReceiveChannel = activeReceiveChannel == 0 ? 1 : 0;
		
	}
	return RTIOSTREAM_NO_ERROR;		
}

void SetupTransmitChannel( data_channel_t* data_channel, size_t size )
{
	DMA_SetupBlock(
	data_channel->ch,
	data_channel->buffer,
	DMA_CH_SRCRELOAD_NONE_gc,
	DMA_CH_SRCDIR_INC_gc,
	(void *) &USARTC0.DATA,
	DMA_CH_DESTRELOAD_NONE_gc,
	DMA_CH_DESTDIR_FIXED_gc,
	size,
	DMA_CH_BURSTLEN_1BYTE_gc,
	0, // Perform once
	0
	);
	data_channel->configured = 1;
	data_channel->read = 0;
	
	DMA_EnableSingleShot( data_channel->ch );
	DMA_SetTriggerSource( data_channel->ch, DMA_CH_TRIGSRC_USARTC0_DRE_gc );
	
	// Set up interrupts
	DMA_SetIntLevel(data_channel->ch, DMA_CH_TRNINTLVL_LO_gc, DMA_CH_ERRINTLVL_LO_gc);
}

void SetupReceiveChannel( data_channel_t* data_channel )
{
	DMA_SetupBlock(
	data_channel->ch,
	(void *) &USARTC0.DATA,
	DMA_CH_SRCRELOAD_NONE_gc,
	DMA_CH_SRCDIR_FIXED_gc,
	data_channel->buffer,
	DMA_CH_DESTRELOAD_NONE_gc,
	DMA_CH_DESTDIR_INC_gc,
	BUFFER_SIZE,
	DMA_CH_BURSTLEN_1BYTE_gc,
	0, // Perform once
	1
	);
	data_channel->completed = 0;
	data_channel->read = 0;
	
	DMA_EnableSingleShot( data_channel->ch );
	DMA_SetTriggerSource( data_channel->ch, DMA_CH_TRIGSRC_USARTC0_RXC_gc );
	
	// Set up interrupts
	//DMA_SetIntLevel(data_channel->ch, DMA_CH_TRNINTLVL_LO_gc, DMA_CH_ERRINTLVL_LO_gc);
}

ISR(DMA_CH0_vect) 
{
	DMA_DisableChannel(TX_DC[0].ch);
	
	// Mark the channel as unconfigured
	TX_DC[0].configured = 0;
	
	// Check if we should start the other channel
	if (TX_DC[1].configured)
	{
		DMA_EnableChannel(TX_DC[1].ch);
	}
	
	// Clear the interrupt flags
	TX_DC[0].ch->CTRLB |= DMA_CH_ERRIF_bm | DMA_CH_TRNIF_bm;
}

ISR(DMA_CH1_vect)
{
	DMA_DisableChannel(TX_DC[1].ch);
	
	// Mark the channel as unconfigured
	TX_DC[1].configured = 0;
	
	// Check if we should start the other channel
	if (TX_DC[0].configured)
	{
		DMA_EnableChannel(TX_DC[0].ch);
	}
	
	// Clear the interrupt flags
	TX_DC[1].ch->CTRLB |= DMA_CH_ERRIF_bm | DMA_CH_TRNIF_bm;
}

ISR(DMA_CH2_vect) 
{

}

ISR(DMA_CH3_vect) 
{	
}
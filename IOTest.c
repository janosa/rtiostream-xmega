/*
 * IOTest.c
 *
 * Created: 27.03.2014 14:45:49
 *  Author: janov_000
 */ 


#include <avr/io.h>
#include <avr/interrupt.h>
#define F_CPU 2000000
#include <util/delay.h>
#include "rtiostream.h"
#include "rtiostreamtest.h"

// static uint8_t buffer[512];

int main(void)
{
	// Set clock to 32Mhz
	OSC.CTRL |= OSC_RC32MEN_bm;
	while(!(OSC.STATUS & OSC_RC32MRDY_bm));
	CCP = 0xD8;
	CLK.CTRL = CLK_SCLKSEL_RC32M_gc;
	CCP = 0xD8;
	CLK.LOCK = CLK_LOCK_bm;
	OSC.CTRL &= ~OSC_RC2MEN_bm;
	
	PMIC.CTRL |= PMIC_HILVLEN_bm | PMIC_MEDLVLEN_bm | PMIC_LOLVLEN_bm;
	sei();
	rtiostreamtest(0, 0);
	/*rtIOStreamOpen(0, 0);
    while(1)
    {
		size_t size = 0;
		rtIOStreamRecv(0, buffer, 512, &size);
		//_delay_ms(10);
		if (size > 0)
		{
			
			rtIOStreamSend(0, buffer, size, &size);
			//_delay_ms(10);
		}
    }*/
}